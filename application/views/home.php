<!DOCTYPE html>
<html>

<head>
	<title>Quotation Generator</title>
	<style>
		/* Style buttons */
		.btn {
			background-color: DodgerBlue;
			border: none;
			color: white;
			padding: 12px 30px;
			cursor: pointer;
			font-size: 20px;
		}

		/* Darker background on mouse-over */
		.btn:hover {
			background-color: RoyalBlue;
		}

	</style>
</head>

<body>
	<div id="container">
		<a href="<?php echo base_url().'index.php/Report/pdf' ?>">
			<button class="btn" style="width:100%">Download Quotation</button>
		</a>
	</div>
</body>

</html>
