<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>QUOTATION</title>

    <style>
        .quotation-box {
            max-width: 800px;
            margin: auto;
            padding: 25px;
            font-size: 16px;
            line-height:normal;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .quotation-box table {
            width: 100%;
            line-height: normal;
            text-align: left;
        }

        .quotation-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .quotation-box table tr.heading td:nth-child(1) {
            text-align: left;
            width: 10px;
        }

        .quotation-box table tr.heading td {
            text-align: left;
        }

        .title_table td {
            width: 100%;
        }


        .quotation-box table tr.top table td {
            padding-bottom: 20px;
			vertical-align:middle;
        }

        .quotation-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
			text-align:left;
            color: #333;
        }

		.quotation-box table tr.top table td.idquota {
			text-align:left;
            color: white;
			background-color:lightblue;
        }


        .quotation-box table tr.information table td {
            padding-bottom: 40px;
        }

        .quotation-box table td:nth-child(2) {
            text-align: right;
        }

        .quotation-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }

        .quotation-box table tr.item td {
            text-align: left;
            font-size: 12px;
        }

        .quotation-box table tr.item td {
            border-bottom: 1px solid #eee;
        }

        .quotation-box table tr.item.last td {
            border-bottom: none;
            text-align: left;
        }

        .quotation-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
            text-align: left;
        }

        .title_table h3 {
            // background-color: lightgray;
			padding:0;
        }

        .remarks ul li {
            font-size: 12px;
            line-height: normal;
			text-align:left;
        }


        @media only screen and (max-width: 600px) {
            .quotation-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .quotation-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }


        }

        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td {
            text-align: left;
        }
    </style>
</head>

<body>
    <div class="quotation-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="4">
                    <table>
                        <tr>
                            <td class="title">
                                <h3>SZETO GROUP</h3>
                            </td>

                            <td class="idquota">
                                ID #: <b>123</b><br>
                                Created: January 1, 2017<br>
                                Due: February 1, 2017
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="information">
                <td colspan="4">
                    <table>
                        <tr>
                            <td>
                                <b>To :</b>
                                PT Marga Dwi Kencana.<br>
                                12345 Jl. Pahlawan<br>
                                Surabaya, IDN 12345
                            </td>

                            <td>
                                <b>Subject:</b> Quotation Rate - Local Charge at Korea <br>
                                and Custom Clearance at Jakarta <br>
                                Attn: Ms. Viona (Import Division)<br>
                                Viona@example.com
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="sub-message">
                <td colspan="4">
                    <table>
                        <tr>
                            <td>
                                Dear Ms.Viona,<br>
                                Please find the item of rate as below :
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="table_content">
                <td colspan="4">
                    <table>
                        <tr class="title_table">
                            <td colspan="4">
                                <h3>Local Charge at Korea</h3>
                            </td>
                        </tr>
                        <tr class="heading">
                            <td>
                                No
                            </td>
                            <td>
                                Kind of Charges
                            </td>
                            <td>
                                Rate(IDR)
                            </td>
                            <td>
                                Remarks
                            </td>
                        </tr>
                        <tr class="item">
                            <td>
                                1
                            </td>
                            <td>
                                Handling Charge
                            </td>
                            <td>
                                Rp 700.000
                            </td>
                            <td>
                                Pershipment
                            </td>
                        </tr>
                        <tr class="item">
                            <td>
                                2
                            </td>
                            <td>
                                Custom Clearance
                            </td>
                            <td>
                                Rp 1.650.000
                            </td>
                            <td>
                                Pershipment
                            </td>
                        </tr>
                        <tr class="item">
                            <td>
                                3
                            </td>
                            <td>
                                Admin Fee & Document Fee
                            </td>
                            <td>
                                Rp 500.000
                            </td>
                            <td>
                                Pershipment
                            </td>
                        </tr>
                        <tr class="item">
                            <td>
                                4
                            </td>
                            <td>
                                Warehouse Fee at Airport
                            </td>
                            <td>
                                Rp 250.000
                            </td>
                            <td>
                                Pershipment
                            </td>
                        </tr>
                        <tr class="item">
                            <td>
                                5
                            </td>
                            <td>
                                Pickup Free to Airport
                            </td>
                            <td>
                                Rp 2.800.000
                            </td>
                            <td>
                                Console(+100 Kgs)
                            </td>
                        </tr>
                        <tr class="item">
                            <td>
                                6
                            </td>
                            <td>
                                Pickup Free to Airport
                            </td>
                            <td>
                                Rp 3.100.000
                            </td>
                            <td>
                                Non-Console (up to 1 ton)
                            </td>
                        </tr>
                        <tr class="item">
                            <td>
                                7
                            </td>
                            <td>
                                X-Ray
                            </td>
                            <td>
                                TBA
                            </td>
                            <td>
                                If Any
                            </td>
                        </tr>
                        

                    </table>
                </td>
            </tr>
            <tr class="table_content">
                <td colspan="4">
                    <table>
                        <tr class="title_table">
                            <td colspan="4">
                                <h3>Custom Clearance Air Port ( LCL-AIR )</h3>
                            </td>
                        </tr>
                        <tr class="heading">
                            <td>
                                No
                            </td>
                            <td>
                                Kind of Charges
                            </td>
                            <td>
                                Rate(IDR)
                            </td>
                            <td>
                                Remarks
                            </td>
                        </tr>
                        <tr class="item">
                            <td>
                                1
                            </td>
                            <td>
                                Custom Clearance
                            </td>
                            <td>
                                Rp 650.000
                            </td>
                            <td>
                                Per shipment up to 300 Lgs
                            </td>
                        </tr>
                        <tr class="item">
                            <td>
                                2
                            </td>
                            <td>
                                Document Handling (Yellow Line)
                            </td>
                            <td>
                                Rp 300.000
                            </td>
                            <td>
                                Pershipment
                            </td>
                        </tr>
                        <tr class="item">
                            <td>
                                3
                            </td>
                            <td>
                                Inspection Charges (Red Line - if needed)
                            </td>
                            <td>
                                Rp 600.000
                            </td>
                            <td>
                                Pershipment, up to 300 Kgs
                            </td>
                        </tr>
                        <tr class="item">
                            <td>
                                4
                            </td>
                            <td>
                                PIB EDI
                            </td>
                            <td>
                                Rp 150.000
                            </td>
                            <td>
                                Pershipment /Document
                            </td>
                        </tr>
                        <tr class="item">
                            <td>
                                5
                            </td>
                            <td>
                                Handling Fee, and Admin Fee
                            </td>
                            <td>
                                Rp 150.000
                            </td>
                            <td>
                                Per Shipment
                            </td>
                        </tr>
                        <tr class="item last">
                            <td>
                                6
                            </td>
                            <td>
                                Trucking Charges to Arcadia
                            </td>
                            <td>
                                Rp 1.100.000
                            </td>
                            <td>
                                Per Trip
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr class="remarks">
                <td colspan="4">
                    <table>
                        <tr>
                            <td>Remarks: </td>
                        </tr>
                        <tr>
                            <td>
                                <ul style=margin:0;>
                                    <li>Destination Charges from Airlines / Agent Forwarder / Warehouse / Storage will
                                        be charges at cost. Will attach the original invoice from said parties.</li>
                                    <li>Rate above valid only for General Cargo (<b>Non Hazardous cargo</b>) till End
                                        of October 2017</li>
                                    <li>The above rate Subject to VAT 10%.</li>
                                    <li>The above rate will be charge whichever is greater (<b>volumetric weight</b>)</li>
                                    <li>The above rate are subject to insurance (<b>if needed</b>)</li>
                                    <li>Even damage goods, due to wrong packaging / without packaging shall not be our
                                        liability</li>
								</ul>
								<p style="page-break-after: always;">&nbsp;</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="notes">
                <td colspan="4">
                    <table>
                        <tr>
                            <td>
                                Hope rate above workable enough to your requirement. <br>
                                We are looking forward your kind support to PT SZETO GLOBAL INDONESIA.
                                <br><br>
                                If any question or further information, please feel free to contact us., thank you
                                <br><br><br>
                                Best Regards,
                                <br><br>
                                <br><br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="signature">
                <td colspan="4">
                    <table>
                        <tr>
                            <td>
                                <b>Devy Arisandi Widjaja</b><br>
                                Marketing Executive
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</body>

</html>
