<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Report extends CI_Controller {
 
	public function pdf()
	{
		$this->load->library('pdfgenerator');
 
		$data['users']=array();
 
	    $html = $this->load->view('quotation',$data,TRUE);
	    
	    $this->pdfgenerator->generate($html,'Quotation');
	}
}
